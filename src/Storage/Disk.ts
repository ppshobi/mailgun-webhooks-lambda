import StorageContract from "Contracts/Storage";
import { writeFile } from 'fs';
import Event from "Contracts/Event";

export default class Disk implements StorageContract 
{
    async store(event: Event): Promise<any> {
        return writeFile(`${event.getKey()}.json`, event.getBody(), function (err) {
            console.log(err);
        });
    }
}