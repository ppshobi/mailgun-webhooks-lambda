import Storage from "Contracts/Storage";
import Event from "Contracts/Event";

import { S3 } from "aws-sdk";

export default class S3Storage implements Storage {
  s3: S3;
  bucket: string;

  constructor(s3: S3) {
    this.s3 = s3;
    this.bucket = process.env.AWS_S3_BUCKET_NAME || "lambda-mailgun-webhooks";
  }

  store(event: Event): Promise<any> {
     return this.s3
        .upload({
          Bucket: this.bucket,
          Key: event.getKey() + ".json",
          Body: event.getBody(),
        }).promise()
  }
  
}

