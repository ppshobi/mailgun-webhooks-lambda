## Usage

1. Install the dependencies using `npm install` 
2. Make sure you have typescript installed and `tsc` is available in your path
3. Compile the source code using `tsc` a `dist` folder should be generated with compiled javascript in it

## Instruction for deploying to Aws Lambda

1. Create a lambda function with following permissions
    1. AmazonS3FullAccess
    2. AmazonSNSFullAccess
2. Create a bucket in S3 and inject the bucket name via environment options in lambda with the name `AWS_S3_BUCKET_NAME`
    1. A default bucket name was used while testing.
3. Create a topic in Aws SNS and inject the `ARN` via the environment variables with the name `SNS_TOPIC_ARN` 
    1.    A default `ARN` was used while testing.
4. Upload the `dist/` folder by zipping it to lambda.
5. Set up an API gateway on `/` with `POST` request pointed to the lambda function.
6. Your lambda function should look like [this](sample-lambda-function.png) in the GUI. 
7. Try a `POST` request to the API gateway endpoint with a sample event from Mailgun.[example event](sample-event-used.json). Or set up a test event in lambda with the contents of [sample-event-given](sample-event-used.json) and trigger that. 
8. you should be able to see a result [similiar to this.](sample-execution-result)
9. A file with the name `<event-id>.json` will be created in the S3 Bucket and a Message will be published to the SNS topic.

## Architecture

- The entry point is `src/index.ts`. 
- Contracts/Interfaces are defined inside the `src/Contracts` directory.
- Concrete implementations are inside the `src/` with the appropriate service name.

## Possible Improvements
- I thought of introducing a simple DI container for injecting the dependencies and binding the abstractions to the interfaces instead of directly newing up the classes in the `/src/index.ts` but, I thought it will be overkill for this task.

- Instead of using a single general `Event` class under `Mailgun/` we could extend the existing `Event.ts` and create appropriate Event classes like `Events/Opened`, `Events/Accepted` etc... so that any additional methods which are local to that specific event can be pushed down there. But since the intention of this task is just to log the event. I kept myself from doing that.

## Caveats
- This was my first lambda experience even though I have heard about it before. 
- Even though I have scratched Typescript's surface, I have never tried writing a library like this before. So the syntax maybe not be very appropriate for the typescript world.
- The structure and the folder names come from my experience with PHP applications I have written before, because of the same reason that I have never written a full-blown typescript application.
- I tried creating a cloud formation template from the existing resources, but then somewhere it started creating EC2 instances, few security groups etc... in the AWS console. So instead I drew [this](cloud-formation-template.png) picture from cloud formation GUI. 
- No unit tests.



