import MailgunEvent from "./Mailgun/Event";
import S3Storage from "./Storage/S3Storage";
import Sns from "./Pubsub/Sns";
import { SNS, S3 } from "aws-sdk";

export const handler = async (event: any = {}): Promise<any> => {
  const mailgunEvent = new MailgunEvent(event);
  
  const awsSns = new SNS();
  const awsS3 = new S3();

  await new S3Storage(awsS3).store(mailgunEvent);
  await new Sns(awsSns).publish(mailgunEvent);
};