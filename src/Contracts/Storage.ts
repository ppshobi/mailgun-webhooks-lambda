import Event from "Contracts/Event";

export default interface Storage {
    store(event: Event): Promise<any>;
    
    //TODO:retrive(key:string): Promise<any>;
}
