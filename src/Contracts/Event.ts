export default interface Event {
    key: string;
    body: string;
    getKey():string;
    getBody():string;
}
