import EventContract from "Contracts/Event";

export default class Event implements EventContract {
    key: string;    
    body: string;

    constructor(data: any) {
        this.key = data['event-data']['id'];
        this.body = JSON.stringify(data);
    }

    getKey(): string {
        return this.key;
    }
    
    getBody(): string {
        return this.body;
    }
    
}
