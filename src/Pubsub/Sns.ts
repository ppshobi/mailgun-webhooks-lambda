import Pubsub from "Contracts/Pubsub";
import Event from "Contracts/Event";
import { SNS } from "aws-sdk";

export default class Sns implements Pubsub {
  sns: SNS;
  topic: string;

  constructor(sns: SNS) {
    this.sns = sns;
    this.topic = process.env.SNS_TOPIC_ARN || "arn:aws:sns:eu-central-1:352110184201:mailgun-webhooks";
  }

  publish(event: Event): Promise<any> {
    return this.sns
      .publish({
        Message: event.getBody(),
        Subject: "Mailgun event from Lambda",
        TopicArn: this.topic
      })
      .promise();
  }
}
