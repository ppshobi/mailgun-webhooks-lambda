import Event from "Contracts/Event";

export default interface Pubsub {
    publish(event: Event): Promise<any>;
}
